# XLSX to elasticsearch uploader
This simple script is simpy to extract the content of an excel file then post it into an elasticsearch instance for indexing.


## installation
Install all dependency
```
(sudo) npm install
```
## Configuration
setup the configuration file at `config.js`
**data** : the relative path to your file.
**dataKeyField** : the column name of the key field for the record inside your excel file.
**uploadData** : a **true** and **false** value field. To indicate whether you want to upload all the file or not.
**elasticsearchUrl** : The url of the elasticsearch server.
**dataType** : The first layer of data categorization
**typeCategory** : The second later of data categorization

## Run the script
Run the script by executing script
```
node index.js
```