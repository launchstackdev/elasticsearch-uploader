'use strict';

module.exports = {
	// Data options
	data : 'us500.xlsx',
	dataKeyField: 'id',


	uploadData: true,
	// Elastic search configuration
	elasticsearchUrl : 'http://elasticsearch.indrasantosa.com:9200',
	// type of information
	dataType: 'data',
	// category of information
	typeCategory: 'company',

};