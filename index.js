'use strict';

var config = require('./config'),
	excelParser = require('excel-parser'),
	request = require('request'),
	path = require('path'),
	_ = require('lodash');

console.log('Parsing file ' + config.data);

excelParser.worksheets({
	inFile: config.data,
}, function(err, worksheets) {

	// For each worksheets, parse and send data
	_.forEach(worksheets, function(sheet) {

		// For each work sheet found parse the header
		var parseHeader = true;
		var recordHeader = [];

		excelParser.parse({
			inFile: config.data,
			worksheet: sheet.id,
			skipEmpty: false,
		}, function(err, records) {
			if(err) console.log(err);

			_.forEach(records, function(record) {
				if(parseHeader) {

					// Populate object header
					_.forEach(record, function(field) {
						recordHeader.push(field);		
					});
					
					// Once done, ready to parse the body
					parseHeader = false;
				} else {
					var obj = new Object();
					// populate object data by making a key/value pair
					var headerLength = recordHeader.length;
					for(var i = 0; i < headerLength; i++) {
						obj[recordHeader[i]] = record[i];
					};

					//console.log(JSON.stringify(obj));

					// Upload file
					if(config.uploadData) {
						var dataPath = path.join(config.dataType , config.typeCategory , obj[config.dataKeyField]);
						var options = {
							url: config.elasticsearchUrl + '/' + dataPath,
							method: "PUT",
							body: JSON.stringify(obj)
						};
						//console.log(JSON.stringify(options));
						
						request(options, function(err, msgObj, resp) {
							if(err) console.log(err);
							JSON.stringify(resp);
						});
						
					}
				}

			});
				


			
		});
	});

});

